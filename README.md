# mirai-console-plugin BilibiliVideoInfoAutoGenerator
一个获取B站视频信息的插件。  
当聊天信息中出现av号和bv号时，获取视频信息并返回消息。  
所有聊天中可用。  
**由于B站API不一定不会修改，若从某时开始该API无法使用，则不会再继续维护该插件。**