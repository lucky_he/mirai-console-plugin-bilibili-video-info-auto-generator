package fun.boomcat.luckyhe.mirai.plugin.bilibilivideoinfoautogenerator;

import fun.boomcat.luckyhe.mirai.plugin.bilibilivideoinfoautogenerator.config.ConfigOperation;
import fun.boomcat.luckyhe.mirai.plugin.bilibilivideoinfoautogenerator.listener.BilibiliMessageListener;
import fun.boomcat.luckyhe.mirai.plugin.bilibilivideoinfoautogenerator.utils.BilibiliUtil;
import fun.boomcat.luckyhe.mirai.plugin.bilibilivideoinfoautogenerator.utils.URLUtil;
import net.mamoe.mirai.console.extension.PluginComponentStorage;
import net.mamoe.mirai.console.plugin.jvm.JavaPlugin;
import net.mamoe.mirai.console.plugin.jvm.JvmPluginDescriptionBuilder;
import net.mamoe.mirai.event.GlobalEventChannel;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

public class BilibiliVideoInfoAutoGenerator extends JavaPlugin  {
    private static final BilibiliVideoInfoAutoGenerator INSTANCE = new BilibiliVideoInfoAutoGenerator();

    public BilibiliVideoInfoAutoGenerator() {
        super(new JvmPluginDescriptionBuilder(
                "luckyhe.bilibilivideoinfoautogenerator",
                "1.2"
        ).build());
    }

    @Override
    public void onEnable() {
        super.onEnable();
        GlobalEventChannel.INSTANCE.registerListenerHost(new BilibiliMessageListener());
    }

    @Override
    public void onDisable() {
        super.onDisable();
    }

    @Override
    public void onLoad(@NotNull PluginComponentStorage $this$onLoad) {
        super.onLoad($this$onLoad);
        BilibiliUtil.init();
        URLUtil.init(getDataFolder().getPath());
        try {
            ConfigOperation.initConfigPath(getConfigFolder(), getResource("config.yml", StandardCharsets.UTF_8));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
