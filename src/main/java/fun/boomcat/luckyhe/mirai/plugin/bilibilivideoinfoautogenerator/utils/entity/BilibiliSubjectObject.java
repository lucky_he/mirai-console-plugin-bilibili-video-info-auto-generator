package fun.boomcat.luckyhe.mirai.plugin.bilibilivideoinfoautogenerator.utils.entity;

import net.mamoe.mirai.Bot;
import net.mamoe.mirai.contact.Contact;

public class BilibiliSubjectObject {
    private final Contact contact;
    private final String bv;
    private final Long av;
    private final Long uid;
    private final Bot bot;

    public BilibiliSubjectObject(Contact contact, Long av, String bv, Long uid, Bot bot) {
        this.contact = contact;
        this.av = av;
        this.bv = bv;
        this.uid = uid;
        this.bot = bot;
    }

    public Contact getContact() {
        return contact;
    }

    public String getBv() {
        return bv;
    }

    public Bot getBot() {
        return bot;
    }

    public Long getAv() {
        return av;
    }

    public Long getUid() {
        return uid;
    }
}
